﻿
cmake_minimum_required (VERSION 3.8)

project (vs-test-explorer-boost-qt5)
enable_testing()


add_executable (vs-test-explorer-boost-qt5 "src/main.cpp")


# Qt5
set(QT_PATH "C:/Qt/5.12.5/msvc2017_64")
set(CMAKE_PREFIX_PATH ${CMAKE_PREFIX_PATH} "${QT_PATH}")
find_package(Qt5Core)


# boost
set(BOOST_MIN_VERSION "1.71.0")
set(Boost_USE_MULTITHREADED   ON)
set(Boost_USE_STATIC_RUNTIME OFF)
if(NOT DEFINED Boost_USE_STATIC_LIBS)
  # use static libs if not specified otherwise
  set(Boost_USE_STATIC_LIBS ON)
endif()
if(NOT ${Boost_USE_STATIC_LIBS})
  # if using shared lib, add define required for boost test
  add_definitions(-DBOOST_TEST_DYN_LINK)
endif()
find_package(Boost ${BOOST_MIN_VERSION} REQUIRED COMPONENTS unit_test_framework REQUIRED)


# tests

link_directories($ENV{VCInstallDir}/Auxiliary/VS/UnitTest/lib)


# tests with no dependencies
add_executable(TestNoDeps test/test_nodeps.cpp)
target_include_directories(TestNoDeps PRIVATE $ENV{VCInstallDir}/Auxiliary/VS/UnitTest/include)
add_test(NAME TestNoDeps COMMAND TestNoDeps)


# tests depending on Qt5
add_executable(TestQt5 test/test_qt5.cpp)
target_include_directories(TestQt5 PRIVATE $ENV{VCInstallDir}/Auxiliary/VS/UnitTest/include)
target_link_libraries(TestQt5 Qt5::Core)
add_test(NAME TestQt5 COMMAND TestQt5)


# simple boost tests
add_executable(TestBoost test/test_boost.cpp)
target_link_libraries(TestBoost Boost::unit_test_framework)
add_test(NAME TestBoost COMMAND TestBoost)



# boost + Qt5 tests
add_executable(TestBoostQt5 test/test_boost_qt5.cpp)
target_link_libraries(TestBoostQt5 Qt5::Core Boost::unit_test_framework)
add_test(NAME TestBoostQt5 COMMAND TestBoostQt5)
