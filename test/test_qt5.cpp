#include <CppUnitTest.h>

#include <QString>


using namespace Microsoft::VisualStudio::CppUnitTestFramework;



TEST_CLASS(TestNoDeps)
{
public:
    TEST_METHOD(MyUnitTest)
    {
        QString s("Hello world");
        Assert::IsFalse(s.isEmpty());
    }
};


int main() {
    return 0;
}
